from pydantic import BaseSettings


import os
import sys



class CommonSettings(BaseSettings):
    APP_NAME: str = "mishi-shopify-api"
    DEBUG_MODE: bool = True
    version: str = f"{sys.version_info.major}.{sys.version_info.minor}"
    PROJECT_ROOT: str = os.path.abspath(os.path.dirname(__file__))


class ServerSettings(BaseSettings):
    HOST: str = "0.0.0.0"
    PORT: int = 9009

class MongoSettings(BaseSettings):
    DB_URL: str = "mongodb+srv://admin:admin@cluster0.etzyf.mongodb.net/mishi?retryWrites=true&w=majority"
    DB_NAME: str = "mishi"


class Settings(CommonSettings, ServerSettings, MongoSettings):
    pass

configuration = Settings()