import uvicorn
from fastapi import FastAPI
from motor.motor_asyncio import AsyncIOMotorClient
from config import configuration

app = FastAPI()


from motor.motor_asyncio import AsyncIOMotorClient
from .config import configuration
from .app import app

@app.on_event('startup')
async def establish_db_conn():
    app.mongodb_client = AsyncIOMotorClient(configuration.DB_URL)
    app.mongodb = app.mongodb_client(configuration.DB_NAME)


@app.on_event("shutdown")
async def shutdown_db_client():
    app.mongodb_client.close()



if __name__ == "__main__":
    uvicorn.run(
        "app:app",
        host = configuration.HOST,
        reload = configuration.DEBUG_MODE,
        port = configuration.PORT
    )