# MishiPay

Time: 3 Hours 
Main Task: Create a python web application which retrieves and stores product information from the Shopify API in a database, and allows users to place orders that are sent to Shopify. 
Must complete [ 65 Points ] 
1. Build a web app using your favourite python web framework, using any database of your choice. 
2. Write code to keep products in your app’s database in sync with the Shopify store using the Shopify API. This could be using a CLI script run with a cron job (provide instructions to run it), a background task or whatever alternate method you prefer. [ 20 points ] 
3. Provide a rest endpoint which returns a list of products stored in the database. [ 15 points ] 
4. Provide a rest endpoint which places an order in the Shopify store using the Shopify API. Store suitable details of the order in the database, making sure to store the final state of the order (failed, succeeded etc.). [ 30 points ] 


Create a private gitlab repository [ https://gitlab.com/ ] with timely commits and add info@mishipay.com as a collaborator (as developer or maintainer) of the repository. 
Note : Code must be pushed at the end of the 3 hours . Any commits pushed after 3 hours will not be considered. 
Bonus Tasks [ 35 Points ] 
1. Test cases [ 20 points ] 
2. Provide an API endpoint to get orders between a range of dates (with pagination) from the database [ 10 points ] 
3. Create a Postman collection (or set up Swagger/OpenAPI or equivalent) to document the API endpoints, as well as a README [ 5 points ] 
Note : Create a new branch for the bonus tasks. Any code related to bonus tasks can be pushed up until 12 hours after completing the must complete tasks. 
On Completion of deadline: 
1. Demonstrate the full solution in the review call.
2. Please share your private github code repository link by adding collaborator as mishipay-ltd ahead of the call. 
Notes: 
1. Details required for a Shopify demo account and apis are provided in the Appendix and can be used for the challenge. 
2. All rights to the developed/shared code are held by you, unless hired by MishiPay. APPENDIX 
API Key 
a38f4a6a8cb713fe2bebdbf3df331f54
Password
3182dcd29ff6c3f6f2dd325ba99b4216
Store url 
mishipaytestdevelopmentemptystore.myshopify.com
Base api url
https://{{API_KEY}}:{{PASSWORD}}@{{STORE}}.myshopify.com/admin/api/



API 
1 . Get Products 
curl --location --request GET 
'https://a38f4a6a8cb713fe2bebdbf3df331f54:3182dcd29ff6c3f6f2dd325ba99b4216@mishipaytestdevelopmentemptystore.myshopify.com/admin/api/2021-01/products.json' 
2. Place Order 
curl --location --request POST 
'https://a38f4a6a8cb713fe2bebdbf3df331f54:3182dcd29ff6c3f6f2dd325ba99b4216@mishipaytestdevelopmentemptystore.myshopify.com/admin/api/2021-01/orders.json' \ --header 'Content-Type: application/json' \ 
--data-raw '{ "order": { 
"line_items": [ 
{ 
"variant_id": 32066662432835, 
"quantity": 2
} 
] 
} 
}'




API DOC : 


# get all orders
GET /order

# post an order
POST /order


mkdir -p app/mishi
touch app/mishi/schema.py
touch app/mishi/service.py
touch app/mishi/__init__.py


